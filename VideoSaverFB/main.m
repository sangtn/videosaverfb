//
//  main.m
//  VideoSaverFB
//
//  Created by Tran Sang on 9/5/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
