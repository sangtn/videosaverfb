//
//  ViewController.h
//  VideoSaverFB
//
//  Created by Tran Sang on 9/5/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "BaseTableView.h"
#import "BaseViewController.h"

@interface ViewController :BaseViewController 

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet BaseTableView *tableView_dloaded;
@property (weak, nonatomic) IBOutlet BaseTableView *tableView_dloader;
@property (weak, nonatomic) IBOutlet BaseTableView *tableView_uploader;
//@property (nonatomic, retain) UINavigationBar *navBar;

@end

