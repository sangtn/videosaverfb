//
//  VSFBFunction.m
//  VideoSaverFB
//
//  Created by Macbook Pro on 9/15/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import "VSFBFunction.h"
#import "ISMessages.h"

@implementation VSFBFunction
+ (UIViewController *)getCurrentViewController {
    UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
    return vc;
}

+ (UIViewController *)visibleViewController:(UIViewController *)rootViewController
{
    if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController;
        return [self visibleViewController:[navigationController.viewControllers lastObject]];
    }
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabController = (UITabBarController *)rootViewController;
        return [self visibleViewController:tabController.selectedViewController];
    }
    if (rootViewController.presentedViewController) {
        return [self visibleViewController:rootViewController.presentedViewController];
    }
    return rootViewController;
}

+ (void)showAlertBannerWithMessage:(NSString *)message withViewController : (UIViewController *) viewController{
    [ISMessages showCardAlertWithTitle:nil
                               message:message
                             iconImage:nil
                              duration:3.f
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeSuccess];
}

+ (NSArray *)getDownloadedVideo {
    NSArray * path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentPath = [path objectAtIndex:0];
    NSString * filePath = [documentPath stringByAppendingPathComponent:@"VideoSaverFB"];
    NSArray * listVideo = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:filePath error:nil];
    return listVideo;
}
@end
