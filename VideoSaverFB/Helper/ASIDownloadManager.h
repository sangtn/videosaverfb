//
//  DownlaodManager.h
//  Campaign
//
//  Created by Anil Can Baykal on 1/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
@interface ASIDownloadManager : ASIHTTPRequest <ASIProgressDelegate, ASIHTTPRequestDelegate>{
    long long totalReceived; 
    long long totalBytes; 
    
    //__block
    ASIHTTPRequest * sentinelRequest;
    NSMutableArray * requests; 
    NSMutableArray * parts; 
    NSMutableArray * partsAvailable; 
    NSMutableArray * paths; 
    
    int totalChunk;
    int numberOfWorkers; 
    
    NSTimeInterval startTime;
    NSString *saveToDir;
}
@property(nonatomic,retain)NSString*saveToDir;
@property(nonatomic, assign) int totalChunk; 
@property(nonatomic, assign) int numberOfWorkers; 
@property(nonatomic, assign) long long totalBytes; 
@property(nonatomic, assign) long long totalReceived; 


@end

@protocol ASIDownloadManagerProgress <ASIProgressDelegate>

@optional
-(void)receivedBytes:(NSNumber*)number forInstance:(ASIDownloadManager*)manager; 
-(void)requestFailed:(ASIDownloadManager*)manager withError:(NSError*)err;
@end
