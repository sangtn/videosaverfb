//
//  VSFBPreference.h
//  VideoSaverFB
//
//  Created by Macbook Pro on 9/15/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VSFBAccountModel.h"

@interface VSFBPreference : NSObject

+ (instancetype) sharedPreference;

@property (strong, nonatomic) VSFBAccountModel * accountModel;
@end
