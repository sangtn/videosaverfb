//
//  DownlaodManager.m
//  Campaign
//
//  Created by Anil Can Baykal on 1/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ASIDownloadManager.h"

#define tmpFile(range)	[NSString stringWithFormat:@"%@~%@", self.downloadDestinationPath, range]
@implementation ASIDownloadManager

@synthesize totalChunk;
@synthesize numberOfWorkers; 
@synthesize totalBytes,totalReceived,saveToDir;
-(id)init{
    
    self = [super init]; 
    
    if(self){
        
        retryCount 	= 15;
        
        totalChunk  	= (int)[[ASINetworkQueue queue] maxConcurrentOperationCount] * 2;
        numberOfWorkers = (int)[[ASINetworkQueue queue] maxConcurrentOperationCount] - 1;
        
        requests 	= [NSMutableArray new]; 
        parts 		= [NSMutableArray new]; 
        paths	 	= [NSMutableArray new]; 
        
        // Will limit bandwidth to the predefined default for mobile applications when WWAN is active.
        // Wi-Fi requests are not affected
        // This method is only available on iOS
        [ASIHTTPRequest setShouldThrottleBandwidthForWWAN:YES];
        
        // Will throttle bandwidth based on a user-defined limit when when WWAN (not Wi-Fi) is active
        // This method is only available on iOS
        [ASIHTTPRequest throttleBandwidthForWWANUsingLimit:14800];
        
        // Will prevent requests from using more than the predefined limit for mobile applications.
        // Will limit ALL requests, regardless of whether Wi-Fi is in use or not - USE WITH CAUTION
        [ASIHTTPRequest setMaxBandwidthPerSecond:ASIWWANBandwidthThrottleAmount];
         
    }       
    
    return self;
}

-(void)fireDownloadAtIndex:(int)index{
    
    NSString * range 	= [partsAvailable objectAtIndex:index];
   // NSLog(@"Range2 :[%@]",range);
    NSString * tmp 		= tmpFile(range);
    NSString * tmptmp 	= [NSString stringWithFormat:@"%@~", tmp]; 
    
    NSArray * ranges 	= [range componentsSeparatedByString:@"-"];
   // NSLog(@"Index1:[%@],index2[%@]",[ranges objectAtIndex:0],[ranges objectAtIndex:1]);
    long long start 			= [[ranges objectAtIndex:0] longLongValue];
    long long end 			= [[ranges objectAtIndex:1] longLongValue]; 
    
    ASIHTTPRequest * req = [ASIHTTPRequest requestWithURL:url];    
    [req setDownloadDestinationPath:tmp];
    [req setTemporaryFileDownloadPath:tmptmp]; 
    [req setAllowResumeForFileDownloads:NO]; // resume will be calculated with parts. 
    req.shouldContinueWhenAppEntersBackground=YES;
    [req setTag:[parts indexOfObject:range]]; 
    
    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithCapacity:2];
    [dict setObject:[NSDate date] forKey:@"start"]; 
    [dict setObject:range forKey:@"range"];
    
    [req setUserInfo:dict]; 
    
    req.delegate = self; 
    req.downloadProgressDelegate = self;     
    
    if(totalBytes>0)//If Server not support range
    [req addRequestHeader:@"Range" value:[NSString stringWithFormat:@"bytes=%lld-%lld", start,end]];
    
    NSLog(@"downloading: %lld-%lld at %d", start, end, req.tag);
    [requests addObject:req]; 
    [req startAsynchronous];       
}

-(void)clearDelegatesAndCancel{
    
    for (ASIHTTPRequest * req in requests){
        [req clearDelegatesAndCancel]; 
    }
    
    [requests removeAllObjects];    
    requests = nil; 
    
    [super clearDelegatesAndCancel]; 
}

-(void)cancel{
    
    for (ASIHTTPRequest * req in requests){
        [req cancel]; 
    }    
    
    [requests removeAllObjects];     
    [super cancel]; 
}

-(void)startSynchronous{
    //who downloads stuff synchoronous anyway???
}

-(void)startAsynchronous{
    
    // sending HEAD Request fails on aws and some other servers....
    sentinelRequest = [ASIHTTPRequest requestWithURL:url];  
    [sentinelRequest retain];
    sentinelRequest.delegate = self;    
    [sentinelRequest  startAsynchronous]; 
    
    NSLog(@"discovering file size....");
}

-(void)downloadAvailableChunk{
    
    @synchronized(self){
        
        if ( [partsAvailable count]) {
            
            [self fireDownloadAtIndex:0];
            [partsAvailable removeObjectAtIndex:0]; 
            
        }
    }
}

-(void)startDownload{
    NSLog(@"StartDownload");
    int chunk = totalBytes/totalChunk; 
    long long start = 0; 
    long long stop = 0; 
    
    [parts removeAllObjects]; 
    partsAvailable = [[NSMutableArray alloc] init];
    //Server not support File size
    if(totalBytes==0){
        NSString * range = [NSString stringWithFormat:@"%lld-%lld", start, stop]; 
        //NSLog(@"Test:%@",range);
        [partsAvailable addObject:range];  
        [parts addObject:range];
    }
    while (stop < totalBytes) {
        
        start = stop; 
        stop  = MIN(chunk+stop, totalBytes); 
        
        if (start!=0)start++; 
        
        NSString * range = [NSString stringWithFormat:@"%lld-%lld", start, stop]; 
        [parts addObject:range];
        //NSLog(@"Search Cache %@ %llu==%d",range,[[[NSFileManager defaultManager] attributesOfItemAtPath:tmpFile(range) error:nil] fileSize],stop-start);
        if ( self.allowResumeForFileDownloads && 
            //[[NSFileManager defaultManager] fileExistsAtPath:tmpFile(range)] && omitted
            [[[NSFileManager defaultManager] attributesOfItemAtPath:tmpFile(range) error:nil] fileSize] == (stop-start+1)){
            
            NSLog(@"found in cache %@", range);
            
            totalReceived  += [[[NSFileManager defaultManager] attributesOfItemAtPath:tmpFile(range) error:nil] fileSize];
            
        } 
        else 
        {
            [partsAvailable addObject:range];
            NSLog(@"ready %@", range); 
        }
    }
    
    
    startTime = [NSDate timeIntervalSinceReferenceDate];
    
    for ( int i = 0; i < numberOfWorkers; i++){
        [self downloadAvailableChunk]; 
    }
    NSLog(@"End Download");
}

- (void)request:(ASIHTTPRequest *)_request didReceiveBytes:(long long)bytes{
    
    /*unsigned long long  part = [_request totalBytesRead];
     unsigned long long  size = [_request contentLength];
     float subProgress = (part/(float)size); 
     */
    totalReceived += bytes;
    if( [downloadProgressDelegate respondsToSelector:@selector(receivedBytes:forInstance:)]){
        [downloadProgressDelegate performSelector:@selector(receivedBytes:forInstance:)
                                       withObject:[NSNumber numberWithLongLong:bytes]
                                       withObject:self]; 
        
    }
}


-(void)requestFailed:(ASIHTTPRequest *)_request{
    
    @synchronized(self){
        
        NSLog(@"Oopps request failed:%@",_request.error);
        NSString * range = [_request.userInfo objectForKey:@"range"];
        
        if ( --retryCount == 0||range==nil) {
            
            NSLog(@"failing with error %@",_request.error);
            [self failWithError:_request.error];
            
            if( [downloadProgressDelegate respondsToSelector:@selector(requestFailed:withError:)]){
                [downloadProgressDelegate performSelector:@selector(requestFailed:withError:)
                                               withObject:self
                                               withObject:error];
            }
                
            [self clearDelegatesAndCancel];
            
        } else {
            
            NSLog(@"will retry chunk %@",range);
            [partsAvailable addObject:range];
            [self downloadAvailableChunk]; // retry another block
            [requests removeObject:_request];
            
        }        
    }
}
-(void)moveFileToDocument:(NSString*)path{
    NSLog(@"moveFileToDocument");
    NSArray*tmp=[path componentsSeparatedByString:@"/"];
    NSString*fileName=[tmp objectAtIndex:[tmp count]-1 ];
    //move path
    NSError*err;
    NSString *docsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    if(self.saveToDir){
        docsDirectory=[docsDirectory stringByAppendingPathComponent:self.saveToDir];
    }
    [[NSFileManager defaultManager] moveItemAtPath:path toPath:[docsDirectory stringByAppendingPathComponent:fileName] error:&err];
    if(err){
        NSLog(@"Cannot move :%@ to :%@",path,[docsDirectory stringByAppendingPathComponent:fileName]);
    }
}
-(void)combineFile:(NSFileHandle*)myHandle atPath:(NSString*)path{
    NSFileHandle *fromHandle = [NSFileHandle fileHandleForReadingAtPath:path];
    while (true) {
        //NSLog(@"Reading %@",path);
        NSData*data=[fromHandle readDataOfLength:1024*1024];
        if([data length]==0)break;
        [myHandle writeData:data];
        
    }
}

-(void)requestFinished:(ASIHTTPRequest *)_request{    
    
    @synchronized(self){
        
        NSString * range = [_request.userInfo objectForKey:@"range"]; 
        NSDate *startDate = [_request.userInfo objectForKey:@"start"];         
        NSTimeInterval  time = [startDate timeIntervalSinceNow];
        
        NSLog(@"finised %@ in %.3f",range, -1*time);
        
        [paths addObject:_request.downloadDestinationPath]; 
        [requests removeObject:_request]; 
        
        // download all parts
        if ( [partsAvailable count])
            return [self downloadAvailableChunk]; 
        
        //wait all request to finish
        if ([requests count ])
            return;
        
        
        NSString * path = downloadDestinationPath; 
        if (![[NSFileManager defaultManager] createFileAtPath:path contents:nil attributes:nil]){
            NSLog(@"file create failed!"); 
            return; 
        }
        
        NSFileHandle *myHandle = [NSFileHandle fileHandleForWritingAtPath:path];
        
        for ( NSString * range in parts){
            
            NSString * tmp = tmpFile(range); 
            /*
             NSData  *data = [[NSFileManager defaultManager] contentsAtPath:tmp];
             
             NSLog(@"appending %d bytes from tmp:%@", [data length], [tmp lastPathComponent]);
             [myHandle writeData:data]; 
             */
            [self combineFile:myHandle atPath:tmp];
            
            NSError * err = nil; 
            [[NSFileManager defaultManager] removeItemAtPath:tmp error:&err];
            if ( err != nil)
                NSLog(@"failed to clean up!\n%@", err);
            
        }
        
        [myHandle closeFile]; 
        [self moveFileToDocument:self.downloadDestinationPath];
        NSLog(@"finished dowloading %llu in %.3f at %@", totalBytes, [NSDate timeIntervalSinceReferenceDate] - startTime, [self.downloadDestinationPath lastPathComponent]);
        
        //all requests are finished
        [self performSelector:@selector(reportFinished)]; 
        
    }
}

- (void)request:(ASIHTTPRequest *)_request didReceiveResponseHeaders:(NSDictionary *)responseHeaders{
    
    if(_request == sentinelRequest) {
        
        
        NSString * totalSize = [_request.responseHeaders objectForKey:@"Content-Length"];  
        NSString*range=[_request.responseHeaders objectForKey:@"Content-Range"];
        totalBytes = [totalSize intValue]; 
        NSLog(@"headers received. file size:%lld, range:%@",totalBytes,range);
        [sentinelRequest clearDelegatesAndCancel]; 
        //Neu file nho hon 2M thi download 1 chunk
        if(totalBytes<2*1024*1024)
        {
            totalChunk=1;
            numberOfWorkers=1;
        }
        if(![[NSFileManager defaultManager] fileExistsAtPath:downloadDestinationPath])
           //&&[[[NSFileManager defaultManager] attributesOfItemAtPath:downloadDestinationPath error:nil] fileSize] !=totalBytes)
            [self startDownload];
        else{
            NSLog(@"Don't need to download again:%@",self.downloadDestinationPath);
            totalReceived=totalBytes;
            [self moveFileToDocument:self.downloadDestinationPath];
            [self performSelector:@selector(reportFinished)];
        }
        
    } else {
        NSString * range = [_request.responseHeaders objectForKey:@"Content-Range"];
        NSLog(@"request range :%@", range);
        
    }
}

-(void)dealloc{
    
    [requests release];
    requests = nil; 
    [parts release]; 
    parts = nil;     
    [partsAvailable release];
    partsAvailable = nil; 
    [sentinelRequest release];
    [super dealloc]; 
}

@end
