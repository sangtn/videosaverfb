//
//  VSFBFunction.h
//  VideoSaverFB
//
//  Created by Macbook Pro on 9/15/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface VSFBFunction : NSObject
+ (UIViewController *) getCurrentViewController;
+ (UIViewController *)visibleViewController:(UIViewController *)rootViewController;
+ (void) showAlertBannerWithMessage : (NSString *) message withViewController : (UIViewController *) viewController;

+ (NSArray *) getDownloadedVideo;
@end
