//
//  VSFBPreference.m
//  VideoSaverFB
//
//  Created by Macbook Pro on 9/15/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import "VSFBPreference.h"

@implementation VSFBPreference
+ (instancetype)sharedPreference {
    static VSFBPreference *sharedPreference = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPreference = [[[self class] alloc] init];
    });
    return sharedPreference;
}

@end
