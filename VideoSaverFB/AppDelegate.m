//
//  AppDelegate.m
//  VideoSaverFB
//
//  Created by Tran Sang on 9/5/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import "AppDelegate.h"
#import "iRateShare.h"
#import "AdsManager.h"
#import "VideoSaveFB.pch"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AppDelegate ()<AdsManagerDelegate>

@property(nonatomic)UIImageView *splashImageView;

@end

@implementation AppDelegate


//iRate App
+ (void)initialize
{
    //rate app
    [iRateShare sharedInstance].applicationBundleID = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"ORL.VideoSaverFB"];//bundle app
    [iRateShare sharedInstance].onlyPromptIfLatestVersion = NO;
    [iRateShare sharedInstance].daysUntilPrompt = 10;
    [iRateShare sharedInstance].usesUntilPrompt = 10;
    //share
    [iRateShare sharedInstance].applicationShareLink = @"http://apple.co/1ZgiLOd";// TT http://apple.co/1ZgiLOd TS http://apple.co/1OrR5NC
    [iRateShare sharedInstance].daysUntilPromptShare = 10;
    [iRateShare sharedInstance].usesUntilPromptShare = 10;
    [iRateShare sharedInstance].maxPromptShare = 10;
    [iRateShare sharedInstance].onceTimeShared = YES;
    
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [Fabric with:@[[Crashlytics class]]];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
#ifdef INCLUDING_ADS
//    self.splashImageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
//    self.splashImageView.image = [UIImage imageNamed:@""];
//    self.splashImageView.contentMode = UIViewContentModeScaleAspectFill;
//    self.splashImageView.center = self.window.rootViewController.view.center;
//    [self.window.rootViewController.view addSubview:self.splashImageView];
//    [self.window.rootViewController.view bringSubviewToFront:self.splashImageView];
    [AdsManager sharedInstance].delegate = self;
    [[AdsManager sharedInstance] PrepareLoadAdsFullScreen];
#endif
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}




-(void)AdvertisingManagerDidLoadAdsManagerInterstitials
{
    if (self.splashImageView.superview != nil) {
        [[AdsManager sharedInstance] ShowAdsFullScreen];
    }
}

-(void)AdvertisingManagerDidHideAdsManagerInterstitials
{
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.splashImageView.alpha = .0f;
                         CGFloat x = -60.0f;
                         CGFloat y = -120.0f;
                         self.splashImageView.frame = CGRectMake(x,
                                                                 y,
                                                                 self.splashImageView.frame.size.width-x,
                                                                 self.splashImageView.frame.size.height-y);
                     } completion:^(BOOL finished){
                         [self.splashImageView removeFromSuperview];
                     }];
}








- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
