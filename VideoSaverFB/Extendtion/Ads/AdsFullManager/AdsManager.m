//
//  AdsManager.m
//  AVTrimCut
//
//  Created by Tran Ngoc Sang on 24/02/16.
//  Copyright (c) 2016 Tran Ngoc Sang. All rights reserved.
//
//

#import "AdsManager.h"

#define adsManager_key @"_adsManager_key_"
#define adsManager_eventCount_key @"_adsManager_eventCount_key_"
#define adsManager_loadCount_key @"_adsManager_loadCount_key_"
#define adsManager_typeAds_key @"_adsManager_typeAds_key_" //Type ADS Showing...

@implementation AdsManager

+ (void)load
{
    [self performSelectorOnMainThread:@selector(sharedInstance) withObject:nil waitUntilDone:NO];
}

+ (AdsManager *)sharedInstance
{
    static AdsManager *sharedInstance = nil;
    if (sharedInstance == nil)
    {
        sharedInstance = [[AdsManager alloc] init];
    }
    return sharedInstance;
}

- (AdsManager *)init
{
    if ((self = [super init]))
    {
#ifdef INCLUDING_ADS
//        STAStartAppSDK* sdk = [STAStartAppSDK sharedInstance];
//        sdk.appID = STARTAPPID;
//        sdk.devID = STARTUSERID;
#endif
        self.eventsUntilPrompt = 1;
        [self performSelectorOnMainThread:@selector(applicationLaunched) withObject:nil waitUntilDone:NO];
    }
    return self;
}

- (void)applicationLaunched
{
    //check if this is a new version
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:adsManager_typeAds_key])
    {
        [defaults setInteger:kAdsManagerUndefined forKey:adsManager_typeAds_key];
        [defaults setInteger:0 forKey:adsManager_loadCount_key];
        [defaults setInteger:0 forKey:adsManager_eventCount_key];
        [defaults synchronize];
    }
}

- (void)logEvent
{
    self.eventCount++;
    if (self.eventCount >= self.eventsUntilPrompt)
    {
        self.eventCount = 0;
        [self ShowAdsFullScreen];
    }
}

-(void)ShowAdsFullScreen
{
#ifdef INCLUDING_ADS
        if ([self.interstitial isReady]) {
            [[UIApplication sharedApplication] setStatusBarHidden:true withAnimation:UIStatusBarAnimationNone];
            
            
                [self.interstitial presentFromRootViewController:[self topViewController]];
            
        }
    
#endif
}


-(void)PrepareLoadAdsFullScreen
{
#ifdef INCLUDING_ADS
        self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:ADMOBID_FULL];
        self.interstitial.delegate = self;
        // Note: Edit SampleConstants.h to update kSampleAdUnitId with your interstitial ad unit id.
        [self.interstitial loadRequest:[self request]];
#endif
}

- (GADRequest *)request {
    GADRequest *request = [GADRequest request];
    //request.testDevices = @[ kGADSimulatorID, @"85f98f35c6187a40a9f1b7771756b6634af77f9c" ];
    return request;
}

- (NSUInteger)typeAds
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:adsManager_typeAds_key];
}

- (void)setTypeAds:(NSUInteger)typeAds
{
    [[NSUserDefaults standardUserDefaults] setInteger:typeAds forKey:adsManager_typeAds_key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSUInteger)eventCount
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:adsManager_eventCount_key];
}

- (void)setEventCount:(NSUInteger)count
{
    [[NSUserDefaults standardUserDefaults] setInteger:count forKey:adsManager_eventCount_key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - GADInterstitialDelegate
-(void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    self.interstitial = ad;
    if ([self.delegate respondsToSelector:@selector(AdvertisingManagerDidLoadAdsManagerInterstitials)]) {
        [self.delegate AdvertisingManagerDidLoadAdsManagerInterstitials];
    }
}

- (void)interstitial:(GADInterstitial *)interstitial didFailToReceiveAdWithError:(GADRequestError *)error {
    self.interstitialStartApp = [[STAStartAppAd alloc] init];
    [self.interstitialStartApp loadAdWithDelegate:self];
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad
{
    [self closeAds];
}

-(void)didLoadAd:(STAAbstractAd *)ad
{
    
}

-(void)didShowAd:(STAAbstractAd *)ad
{
    
}

-(void)failedLoadAd:(STAAbstractAd *)ad withError:(NSError *)error
{
    [self closeAds];
}

-(void)failedShowAd:(STAAbstractAd *)ad withError:(NSError *)error
{
    [self closeAds];
}

-(void)didCloseAd:(STAAbstractAd *)ad
{
    [self closeAds];
}

-(void)didClickAd:(STAAbstractAd *)ad
{
    [self closeAds];
}

-(void)didCloseInAppStore:(STAAbstractAd *)ad
{
    [self closeAds];
}

-(void)closeAds
{
    [[UIApplication sharedApplication] setStatusBarHidden:false withAnimation:UIStatusBarAnimationNone];
    if ([self.delegate respondsToSelector:@selector(AdvertisingManagerDidHideAdsManagerInterstitials)]) {
        [self.delegate AdvertisingManagerDidHideAdsManagerInterstitials];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self PrepareLoadAdsFullScreen];
    });
}

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}


- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
        
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
     
}



@end
