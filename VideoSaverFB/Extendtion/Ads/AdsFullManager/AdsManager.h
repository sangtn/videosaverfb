//
//  AdsManager.h
//  AVTrimCut
//
//  Created by Tran Ngoc Sang on 24/02/16.
//  Copyright (c) 2016 Tran Ngoc Sang. All rights reserved.
//
//
//

#import <Foundation/Foundation.h>
#import <StartApp/StartApp.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
//#import <TapForTap/TFTTapForTap.h>

#define kAdsManagerAdmobs 0
//#define kAdsManageriAd 1
//#define kAdsManagerStartApp 2
//#define kAdsManagerTFT 3
#define kAdsManagerUndefined 4

@protocol AdsManagerDelegate <NSObject>
@required
- (void)AdvertisingManagerDidLoadAdsManagerInterstitials;
- (void)AdvertisingManagerDidHideAdsManagerInterstitials;
@end

@interface AdsManager : NSObject <GADInterstitialDelegate,STADelegateProtocol>

+ (AdsManager *)sharedInstance;

- (void)logEvent;

@property(nonatomic, retain) STAStartAppAd *interstitialStartApp;
@property(nonatomic, retain) GADInterstitial *interstitial;
@property (nonatomic) id<AdsManagerDelegate> delegate;
@property (nonatomic, assign) NSUInteger eventsUntilPrompt;

@property (nonatomic) NSUInteger eventCount;
-(void)PrepareLoadAdsFullScreen;
-(void)ShowAdsFullScreen;

@end
