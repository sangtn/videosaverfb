//
//  SettingViewController.m
//  MusicTube
//
//  Created by Tran Ngoc Sang on 24/02/16.
//  Copyright (c) 2016 Tran Ngoc Sang. All rights reserved.
//

#import "SettingViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "SVModalWebViewController.h"
#import "FMConstants.h"
#import "LabelConstants.h"
#import "VENTouchLockCreatePasscodeViewController.h"
#import "VENTouchLock.h"
#import "VideoSaveFB.pch"
#import "AdsManager.h"
#import "UIColor+UIColorAdditions.h"
#import "UIImage+UIImageAdditions.h"



@interface SettingViewController ()<MFMailComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate,GADBannerViewDelegate>

@property(nonatomic)UITableView *tableView;
@property(nonatomic)NSMutableArray *arrSection;


@end

@implementation SettingViewController
{
    GADBannerView *bannerView_;
}



-(void)loadView
{
    
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    [self.view setAutoresizesSubviews:YES];
    [self.view setAutoresizingMask:(UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight)];
    //self.view.backgroundColor = [UIColor colorWithRed:0.13 green:0.14 blue:0.16 alpha:1.0];
    
    
    /*// Create the colors
    UIColor *topColor = [UIColor colorWithRed:0.13 green:0.14 blue:0.16 alpha:1.0];
    UIColor *centerColor = [UIColor colorWithRed:0.62 green:0.55 blue:0.5 alpha:1.0];
    UIColor *bottomColor = [UIColor colorWithRed:0.13 green:0.14 blue:0.16 alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
    theViewGradient.colors = [NSArray arrayWithObjects: (id)topColor.CGColor,(id)centerColor.CGColor, (id)bottomColor.CGColor, nil];
    theViewGradient.frame = self.view.bounds;
    
    //Add gradient to view
    [self.view.layer insertSublayer:theViewGradient atIndex:0];
    */
    
    self.navigationItem.title = @"Settings";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithPatternImage:[UIImage imageWithStartColor:[UIColor colorWithHex:0x1f1c27] endColor:[UIColor colorWithHex:0x544a57] size:CGSizeMake(1, 70)]];
    
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneViewController:)];
    [self.navigationItem setRightBarButtonItem:doneItem animated:YES];
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.bounds.size.height) style:UITableViewStyleGrouped];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tableView setAutoresizesSubviews:YES];
    [_tableView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight)];
    [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [self.view addSubview: _tableView];
    
    
    self.arrSection = [NSMutableArray array];
//    [self.arrSection addObject:@[APP_PASSCODE]];
//#ifdef INCLUDING_ADS
//    [self.arrSection addObject:@[UPGRADE_VERSION,HELP]];
//#else
    [self.arrSection addObject:@[HELP]];
//#endif
    [self.arrSection addObject:@[RATING,FEEDBACK]];
    [self.arrSection addObject:@[@"Clear Cache"]];
    [self.arrSection addObject:@[@"Follow @Me on Twitter",@"Follow @Me on Facebook",@"Share us on Facebook",@"Subscribe @Me on Youtube",@"Try other App made by us"]];//

#ifdef INCLUDING_ADS
//    if ((UI_USER_INTERFACE_IDIOM() == IS_IPHONE_5) || UI_USER_INTERFACE_IDIOM() == IS_IPHONE_4) {
//        [self.arrSection addObject:@[@""]];
//    }
    
    [self loadAds];
#endif
    
}

-(void)doneViewController:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)loadAds
{
#ifdef INCLUDING_ADS
    GADRequest *request = [GADRequest request];
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        [bannerView_ setFrame:CGRectMake(bannerView_.frame.origin.x, self.view.frame.size.height-120, self.view.frame.size.width, bannerView_.frame.size.height)];
    }else{
        bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
        [bannerView_ setFrame:CGRectMake(bannerView_.frame.origin.x, self.view.frame.size.height-30, self.view.frame.size.width, bannerView_.frame.size.height)];
    }
    bannerView_.adUnitID = ADMOBID;
    bannerView_.delegate = self;
    bannerView_.rootViewController = self;
    [bannerView_ loadRequest:request];
    [self.view addSubview:bannerView_];
#endif
}



-(void)viewDidAppear:(BOOL)animated
{
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithStartColor:[UIColor colorWithHex:0x1f1c27] endColor:[UIColor colorWithHex:0x544a57] size:CGSizeMake(1, self.view.bounds.size.height)]];
    
    
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}


#pragma mark -
#pragma mark UITableView datasource methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0){
        if(indexPath.row == 0) {
        }
    }
    else if(indexPath.section==1){
        if([[self.arrSection objectAtIndex:indexPath.section][indexPath.row] isEqualToString:HELP]){
            
            SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithAddress:HELP_WEBSITE];
            
//            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//            STTabbarController * tbc = (STTabbarController *)appDelegate.window.rootViewController;
//            if (tbc.secondViewController != nil) {
//                [tbc.secondViewController presentViewController:webViewController animated:YES completion:NULL];
//            }else {
                [self.view.window.rootViewController presentViewController:webViewController animated:YES completion:nil];
//            }
            
        } else {
            static NSString *const iOSAppStoreURLFormat = @"itms-apps://itunes.apple.com/app/id%@";
            static NSString *const iOS7AppStoreURLFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@";
            NSURL *temp = [NSURL URLWithString:[NSString stringWithFormat:([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f)? iOS7AppStoreURLFormat: iOSAppStoreURLFormat, PROID]]; // Would contain the right link
            [[UIApplication sharedApplication] openURL:temp];
        }
    }
    else if(indexPath.section == 2){
        if(indexPath.row == 0){
            static NSString *const iOSAppStoreURLFormat = @"itms-apps://itunes.apple.com/app/id%@";
            static NSString *const iOS7AppStoreURLFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@";
            NSURL *temp = [NSURL URLWithString:[NSString stringWithFormat:([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f)? iOS7AppStoreURLFormat: iOSAppStoreURLFormat, RATING_APPID]]; // Would contain the right link
            [[UIApplication sharedApplication] openURL:temp];
        } else if(indexPath.row == 1){
            if ([MFMailComposeViewController canSendMail]) {
                MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
                [composeViewController setMailComposeDelegate:self];
                [composeViewController setToRecipients:@[EMAIL_SUPPORT]];
                [composeViewController setSubject:EMAIL_SUPPORT_SUBJECT];
                [composeViewController setMessageBody:[[@"\n\n\n\n " stringByAppendingString:[NSString stringWithFormat:@"\n%@",[self appNameAndVersionNumberDisplayString]]] stringByAppendingString:[NSString stringWithFormat:@"\nModel:%@ (%f)",[[UIDevice currentDevice] model],[[UIDevice currentDevice].systemVersion floatValue]]] isHTML:NO];
                [[[composeViewController navigationItem] leftBarButtonItem]setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
                [[[composeViewController navigationItem] leftBarButtonItem]setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateHighlighted];
                [[[composeViewController navigationItem] rightBarButtonItem]setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
                [[[composeViewController navigationItem] rightBarButtonItem]setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateHighlighted];
                
                [self setMFMailFieldAsFirstResponder:composeViewController.view mfMailField:@"_MFComposeSubjectView"];
                
                
//                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//                STTabbarController * tbc = (STTabbarController *)appDelegate.window.rootViewController;
//                if (tbc.secondViewController != nil) {
//                    [tbc.secondViewController presentViewController:composeViewController animated:YES completion:NULL];
//                }else {
                    [self.view.window.rootViewController presentViewController:composeViewController animated:YES completion:nil];
//                }
                
            } else {
                [[[UIAlertView alloc]initWithTitle:EMAIL message:ALERT_UNCONFIG_MAIL delegate:self cancelButtonTitle:OKS otherButtonTitles: nil] show];
            }
        }
    }
    else if(indexPath.section == 3){
        for (NSString *fileName in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:nil]){
            [[NSFileManager defaultManager] removeItemAtPath:[NSTemporaryDirectory()stringByAppendingPathComponent:fileName] error:nil];
        }
    }
    /*else if(indexPath.section == 4){
        if (indexPath.row==0){
            //other app
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://apple.co/1OrR5NC"]];//TS http://apple.co/1OrR5NC // TT http://apple.co/1ZgiLOd
        }
    }*/
    else if(indexPath.section == 4){
        if (indexPath.row==0) {
            //follow in twitter
            NSURL * twitterUrl = [NSURL URLWithString:@"twitter://user?screen_name=tranngocsang"];//https://twitter.com/tranngocsang
            if ([[UIApplication sharedApplication]canOpenURL:twitterUrl]) {
                [[UIApplication sharedApplication]openURL:twitterUrl];
            }else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/tranngocsang"]];
            }
        }else if (indexPath.row==1){
            //follow in facebook
            NSURL * facebookUrl = [NSURL URLWithString:@"fb://profile/240854149372399"];
            if ([[UIApplication sharedApplication]canOpenURL:facebookUrl]) {
                [[UIApplication sharedApplication]openURL:facebookUrl];
            }else {
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://www.facebook.com/orliosapp/"]];
            }
        } else if (indexPath.row==2){
            //share on your facebook
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//                SLComposeViewController * facebookShare = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//                [facebookShare setInitialText:@"Get More!!! http://apple.co/1ZgiLOd"];
//                [facebookShare addURL:[NSURL URLWithString:@"http://apple.co/1ZgiLOd"]];
//                

                NSURL *temp = [NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/us/app/apple-store/id%@?mt=8",RATING_APPID]];
                
                SLComposeViewController * facebookShare = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                [facebookShare setInitialText:@"VideoSaverFB"];
                [facebookShare addURL:temp];
                
//                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//                STTabbarController * tbc = (STTabbarController *)appDelegate.window.rootViewController;
//                if (tbc.secondViewController != nil) {
//                    [tbc.secondViewController presentViewController:facebookShare animated:YES completion:NULL];
//                }else {
                    [self.view.window.rootViewController presentViewController:facebookShare animated:YES completion:nil];
//                }
            }
        }else if (indexPath.row==3){
            //other app
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.youtube.com/channel/UCKa_q8lD3RVvCM8TFgM-A0g"]];
        }else if (indexPath.row==4){
            //other app
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://apple.co/1ZgiLOd"]];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString *)appNameAndVersionNumberDisplayString {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appDisplayName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    return [NSString stringWithFormat:@"%@, Version %@ (%@)",
            appDisplayName, majorVersion, minorVersion];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.arrSection count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [(NSArray*)self.arrSection[section] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    //if(section==5) return @"DISABLE DATA COLLECTION";
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5.0;
}

-(NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    //if(section==5) return @"In order to help us improve our app, we would like to collect anonymous usage data regarding the app's various functions. This data will not be open to the public.";
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return (iPad?50:35);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentify = [NSString stringWithFormat:@"LeftDemoTable%ld",(long)indexPath.section];
    UITableViewCell *cell;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentify];
        [cell setBackgroundColor:[UIColor colorWithRed:0.157 green:0.157 blue:0.157 alpha:.3]];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        cell.textLabel.text = self.arrSection[indexPath.section][indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:(iPad?16:15)];
        cell.textLabel.textColor = [UIColor colorWithHex:0xff46c7];
        /*if(indexPath.section==0){
            if(indexPath.row == 0) {
                [cell.imageView setImage:[UIImage imageNamed:@"Setting.bundle/icon-passcode"]];
                UISwitch *swit = [[UISwitch alloc] init];
                [swit setOn:[[VENTouchLock sharedInstance] isPasscodeSet]];
                [swit addTarget:self action:@selector(actionPasscode:) forControlEvents:UIControlEventValueChanged];
                cell.accessoryView = swit;
            }
        } else*/ if(indexPath.section==0){
            if([[self.arrSection objectAtIndex:indexPath.section][indexPath.row] isEqualToString:HELP]){
                [cell.imageView setImage:[UIImage imageNamed:@"Setting.bundle/icon-help"]];
            } else {
#ifdef INCLUDING_ADS
                [cell.imageView setImage:[UIImage imageNamed:@"Setting.bundle/icon-pro"]];
#endif
            }
        } else if(indexPath.section==1){
            if(indexPath.row == 0){
                [cell.imageView setImage:[UIImage imageNamed:@"Setting.bundle/icon-hearts"]];
            } else if(indexPath.row == 1){
                [cell.imageView setImage:[UIImage imageNamed:@"Setting.bundle/icon-message"]];
            }
        } else if(indexPath.section==2){
            if(indexPath.row == 0){
                [cell.imageView setImage:[UIImage imageNamed:@"Setting.bundle/icon-cache"]];
            }
        }else if(indexPath.section==3){
            if(indexPath.row == 0){
                [cell.imageView setImage:[UIImage imageNamed:@"Setting.bundle/icon-tw"]];
            }else if(indexPath.row == 1){
                [cell.imageView setImage:[UIImage imageNamed:@"Setting.bundle/icon-fa"]];
            }else if(indexPath.row == 2){
                [cell.imageView setImage:[UIImage imageNamed:@"Setting.bundle/icon-sh"]];
            }else if(indexPath.row == 3){
                [cell.imageView setImage:[UIImage imageNamed:@"Setting.bundle/icon-yt"]];
            }else if(indexPath.row == 4){
                [cell.imageView setImage:[UIImage imageNamed:@"Setting.bundle/icon-ho"]];
            }
        }/* else if(indexPath.section==5){
            UISwitch *swit = [[UISwitch alloc] init];
            [swit setOn:[[[NSUserDefaults standardUserDefaults] objectForKey:@"_gaManager_key_"] boolValue]];
            [swit addTarget:self action:@selector(actionGAManager:) forControlEvents:UIControlEventValueChanged];
            cell.accessoryView = swit;
        }*/
    }
    return cell;
}

#pragma mark -
-(void)actionPasscode:(id)sender
{
    if([sender isOn]){
        VENTouchLockCreatePasscodeViewController *vc = [[VENTouchLockCreatePasscodeViewController alloc]init];
        [self.view.window.rootViewController presentViewController:[vc embeddedInNavigationController] animated:YES completion:nil];
    } else {
        if ([[VENTouchLock sharedInstance] isPasscodeSet]) {
            [[VENTouchLock sharedInstance] deletePasscode];
        }
    }
}

-(void)actionGAManager:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:[sender isOn]] forKey:@"_gaManager_key_"];
}
#pragma mark - MailDelegate
-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    //Add an alert in case of failure
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL) setMFMailFieldAsFirstResponder:(UIView*)view mfMailField:(NSString*)field{
    for (UIView *subview in view.subviews) {
        NSString *className = [NSString stringWithFormat:@"%@", [subview class]];
        if ([className isEqualToString:field])
        {
            //Found the sub view we need to set as first responder
            [subview becomeFirstResponder];
            return YES;
        }
        if ([subview.subviews count] > 0) {
            if ([self setMFMailFieldAsFirstResponder:subview mfMailField:field]){
                //Field was found and made first responder in a subview
                return YES;
            }
        }
    }
    //field not found in this view.
    return NO;
}

-(void)viewWillAppear:(BOOL)animated{
    
    //[[AdsManager sharedInstance] logEvent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
