//
//  SettingViewController.h
//  MusicTube
//
//  Created by Tran Ngoc Sang on 24/02/16.
//  Copyright (c) 2016 Tran Ngoc Sang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController
@property(nonatomic,strong)id<UIPageViewControllerDelegate> delegate;
@end

