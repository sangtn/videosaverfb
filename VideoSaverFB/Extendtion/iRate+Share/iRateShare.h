//
//  iRateShare.h
//  testShare+iRate
//
//  Created by Tran Ngoc Sang on 3/17/14.
//  Copyright (c) 2014 Tran Ngoc Sang. All rights reserved.
//

#import <Availability.h>
#import <Foundation/Foundation.h>
#undef weak_delegate
#if __has_feature(objc_arc_weak) && \
(TARGET_OS_IPHONE || __MAC_OS_X_VERSION_MIN_REQUIRED >= __MAC_10_8)
#define weak_delegate weak
#else
#define weak_delegate unsafe_unretained
#endif


#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#else
#import <Cocoa/Cocoa.h>
#endif

#if IRATE_USE_STOREKIT
#import <StoreKit/StoreKit.h>
#endif

extern NSUInteger const iRateAppStoreGameGenreID;
extern NSString *const iRateErrorDomain;


//localisation string keys
static NSString *const iRateMessageTitleKey = @"iRateMessageTitle";
static NSString *const iRateAppMessageKey = @"iRateAppMessage";
static NSString *const iRateGameMessageKey = @"iRateGameMessage";
static NSString *const iRateCancelButtonKey = @"iRateCancelButton";
static NSString *const iRateRemindButtonKey = @"iRateRemindButton";
static NSString *const iRateRate5ButtonKey = @"iRateRate5Button";
static NSString *const iRateRate1ButtonKey = @"iRateRate1Button";
static NSString *const iRateRate1MessageKey = @"iRateRate1Message";
static NSString *const iRateRate1CancelBtnKey = @"iRateRate1CancelBtn";

static NSString *const iShareFacebookButtonKey = @"iShareFacebookButton";
static NSString *const iShareTwitterButtonKey = @"iShareTwitterButton";
static NSString *const iShareTitleKey = @"iShareTitle";
static NSString *const iShareMessageKey = @"iShareMessage";
static NSString *const iShareTextKey = @"iShareText";



typedef enum
{
    iRateErrorBundleIdDoesNotMatchAppStore = 1,
    iRateErrorApplicationNotFoundOnAppStore,
    iRateErrorApplicationIsNotLatestVersion
}
iRateErrorCode;


@protocol iRateShareDelegate <NSObject>
@optional

- (void)iRateCouldNotConnectToAppStore:(NSError *)error;
- (void)iRateDidDetectAppUpdate;
- (BOOL)iRateShouldPromptForRating;
- (void)iRateDidPromptForRating;
- (void)iRateUserDidAttemptToRateApp;
- (void)iRateUserDidDeclineToRateApp;
- (void)iRateUserDidRequestReminderToRateApp;
- (BOOL)iRateShouldOpenAppStore;
- (void)iRateDidPresentStoreKitModal;
- (void)iRateDidDismissStoreKitModal;

@end


@interface iRateShare : NSObject

+ (iRateShare *)sharedInstance;

//iShare
@property (nonatomic, copy) UIImage  *applicationShareImage;
@property (nonatomic, copy) NSString *applicationShareText;
@property (nonatomic, copy) NSString *applicationShareLink;

@property (nonatomic, copy) NSString *facebookButtonLabel;
@property (nonatomic, copy) NSString *twitterButtonLabel;
@property (nonatomic, copy) NSString *shareMessage;
@property (nonatomic, copy) NSString *shareTitle;

//usage settings - these have sensible defaults
@property (nonatomic, assign) NSUInteger usesUntilPromptShare;
@property (nonatomic, assign) float daysUntilPromptShare;
@property (nonatomic, assign) float usesPerWeekForPromptShare;
//with value = 1 promt only one, more than  2.3.4.5.6....
@property (nonatomic, assign) NSUInteger maxPromptShare;
@property (nonatomic) BOOL onceTimeShared;

@property (nonatomic, assign) NSUInteger usesCountShare;
@property (nonatomic, readonly) float usesPerWeekShare;
@property (nonatomic, assign) NSUInteger sharedThisVersionCount;
@property (nonatomic, strong) NSDate *firstUsedShared;
@property (nonatomic) BOOL sharedThisVersion;


//app store ID - this is only needed if your
//bundle ID is not unique between iOS and Mac app stores
@property (nonatomic, assign) NSUInteger appStoreID;

//application details - these are set automatically
@property (nonatomic, assign) NSUInteger appStoreGenreID;
@property (nonatomic, copy) NSString *appStoreCountry;
@property (nonatomic, copy) NSString *applicationName;
@property (nonatomic, copy) NSString *applicationVersion;
@property (nonatomic, copy) NSString *applicationBundleID;

//usage settings - these have sensible defaults
@property (nonatomic, assign) NSUInteger usesUntilPrompt;
@property (nonatomic, assign) NSUInteger eventsUntilPrompt;
@property (nonatomic, assign) float daysUntilPrompt;
@property (nonatomic, assign) float usesPerWeekForPrompt;
@property (nonatomic, assign) float remindPeriod;

//message text, you may wish to customise these
@property (nonatomic, copy) NSString *messageTitle;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *cancelButtonLabel;
@property (nonatomic, copy) NSString *remindButtonLabel;
@property (nonatomic, copy) NSString *rate5ButtonLabel;
@property (nonatomic, copy) NSString *rate1ButtonLabel;

//debugging and prompt overrides
@property (nonatomic, assign) BOOL useAllAvailableLanguages;
@property (nonatomic, assign) BOOL promptAgainForEachNewVersion;
@property (nonatomic, assign) BOOL onlyPromptIfLatestVersion;
@property (nonatomic, assign) BOOL onlyPromptIfMainWindowIsAvailable;
@property (nonatomic, assign) BOOL promptAtLaunch;
@property (nonatomic, assign) BOOL verboseLogging;
@property (nonatomic, assign) BOOL previewMode;

//advanced properties for implementing custom behaviour
@property (nonatomic, strong) NSURL *ratingsURL;
@property (nonatomic, strong) NSDate *firstUsed;
@property (nonatomic, strong) NSDate *lastReminded;
@property (nonatomic, assign) NSUInteger usesCount;
@property (nonatomic, assign) NSUInteger eventCount;
@property (nonatomic, readonly) float usesPerWeek;
@property (nonatomic, assign) BOOL declinedThisVersion;
@property (nonatomic, readonly) BOOL declinedAnyVersion;
@property (nonatomic, assign) BOOL ratedThisVersion;
@property (nonatomic, readonly) BOOL ratedAnyVersion;
@property (nonatomic, weak_delegate) id<iRateShareDelegate> delegate;

//manually control behaviour
- (BOOL)shouldPromptForRating;
- (void)promptForRating;
- (void)promptIfNetworkAvailable;
- (BOOL)openRatingsPageInAppStore;
- (void)logEvent:(BOOL)deferPrompt;


@end
