/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import <Foundation/Foundation.h>
#import "SDWebImageCompat.h"
#import "SDWebImageOperation.h"

typedef NS_OPTIONS(NSUInteger, SDWebImageLoaderOptions) {
    SDWebImageLoaderLowPriority = 1 << 0,
    SDWebImageLoaderProgressiveLoad = 1 << 1,

    /**
     * By default, request prevent the use of NSURLCache. With this flag, NSURLCache
     * is used with default policies.
     */
    SDWebImageLoaderUseNSURLCache = 1 << 2,


    SDWebImageLoaderIgnoreCachedResponse = 1 << 3,
    /**
     * In iOS 4+, continue the load of the image if the app goes to background. This is achieved by asking the system for
     * extra time in background to let the request finish. If the background task expires the operation will be cancelled.
     */

    SDWebImageLoaderContinueInBackground = 1 << 4,

    /**
     * Handles cookies stored in NSHTTPCookieStore by setting 
     * NSMutableURLRequest.HTTPShouldHandleCookies = YES;
     */
    SDWebImageLoaderHandleCookies = 1 << 5,

    /**
     * Enable to allow untrusted SSL certificates.
     * Useful for testing purposes. Use with caution in production.
     */
    SDWebImageLoaderAllowInvalidSSLCertificates = 1 << 6,

    /**
     * Put the image in the high priority queue.
     */
    SDWebImageLoaderHighPriority = 1 << 7,
};

typedef NS_ENUM(NSInteger, SDWebImageLoaderExecutionOrder) {
    /**
     * Default value. All load operations will execute in queue style (first-in-first-out).
     */
    SDWebImageLoaderFIFOExecutionOrder,

    /**
     * All load operations will execute in stack style (last-in-first-out).
     */
    SDWebImageLoaderLIFOExecutionOrder
};

extern NSString *const SDWebImageLoadStartNotification;
extern NSString *const SDWebImageLoadStopNotification;

typedef void(^SDWebImageLoaderProgressBlock)(NSInteger receivedSize, NSInteger expectedSize);

typedef void(^SDWebImageLoaderCompletedBlock)(UIImage *image, NSData *data, NSError *error, BOOL finished);

typedef NSDictionary *(^SDWebImageLoaderHeadersFilterBlock)(NSURL *url, NSDictionary *headers);

/**
 * Asynchronous loader dedicated and optimized for image loading.
 */
@interface SDWebImageLoader : NSObject

/**
 * Decompressing images that are loaded and cached can improve performance but can consume lot of memory.
 * Defaults to YES. Set this to NO if you are experiencing a crash due to excessive memory consumption.
 */
@property (assign, nonatomic) BOOL shouldDecompressImages;

@property (assign, nonatomic) NSInteger maxConcurrentLoads;

/**
 * Shows the current amount of loads that still need to be loaded
 */
@property (readonly, nonatomic) NSUInteger currentLoadCount;


/**
 *  The timeout value (in seconds) for the load operation. Default: 15.0.
 */
@property (assign, nonatomic) NSTimeInterval loadTimeout;


/**
 * Changes load operations execution order. Default value is `SDWebImageLoaderFIFOExecutionOrder`.
 */
@property (assign, nonatomic) SDWebImageLoaderExecutionOrder executionOrder;

/**
 *  Singleton method, returns the shared instance
 *
 *  @return global shared instance of loader class
 */
+ (SDWebImageLoader *)sharedLoader;

/**
 *  Set the default URL credential to be set for request operations.
 */
@property (strong, nonatomic) NSURLCredential *urlCredential;

/**
 * Set username
 */
@property (strong, nonatomic) NSString *username;

/**
 * Set password
 */
@property (strong, nonatomic) NSString *password;

/**
 * Set filter to pick headers for loading image HTTP request.
 *
 * This block will be invoked for each loading image request, returned
 * NSDictionary will be used as headers in corresponding HTTP request.
 */
@property (nonatomic, copy) SDWebImageLoaderHeadersFilterBlock headersFilter;

/**
 * Set a value for a HTTP header to be appended to each load HTTP request.
 *
 * @param value The value for the header field. Use `nil` value to remove the header.
 * @param field The name of the header field to set.
 */
- (void)setValue:(NSString *)value forHTTPHeaderField:(NSString *)field;

/**
 * Returns the value of the specified HTTP header field.
 *
 * @return The value associated with the header field field, or `nil` if there is no corresponding header field.
 */
- (NSString *)valueForHTTPHeaderField:(NSString *)field;

/**
 * Sets a subclass of `SDWebImageLoaderOperation` as the default
 * `NSOperation` to be used each time SDWebImage constructs a request
 * operation to load an image.
 *
 * @param operationClass The subclass of `SDWebImageLoaderOperation` to set
 *        as default. Passing `nil` will revert to `SDWebImageLoaderOperation`.
 */
- (void)setOperationClass:(Class)operationClass;

/**
 * Creates a SDWebImageLoader async loader instance with a given URL
 *
 * The delegate will be informed when the image is finish loaded or an error has happen.
 *
 * @see SDWebImageLoaderDelegate
 *
 * @param url            The URL to the image to load
 * @param options        The options to be used for this load
 * @param progressBlock  A block called repeatedly while the image is loading
 * @param completedBlock A block called once the load is completed.
 *                       If the load succeeded, the image parameter is set, in case of error,
 *                       error parameter is set with the error. The last parameter is always YES
 *                       if SDWebImageLoaderProgressiveLoad isn't use. With the
 *                       SDWebImageLoaderProgressiveLoad option, this block is called
 *                       repeatedly with the partial image object and the finished argument set to NO
 *                       before to be called a last time with the full image and finished argument
 *                       set to YES. In case of error, the finished argument is always YES.
 *
 * @return A cancellable SDWebImageOperation
 */
- (id <SDWebImageOperation>)loadImageWithURL:(NSURL *)url
                                         options:(SDWebImageLoaderOptions)options
                                        progress:(SDWebImageLoaderProgressBlock)progressBlock
                                       completed:(SDWebImageLoaderCompletedBlock)completedBlock;

/**
 * Sets the load queue suspension state
 */
- (void)setSuspended:(BOOL)suspended;

/**
 * Cancels all load operations in the queue
 */
- (void)cancelAllLoads;

@end
