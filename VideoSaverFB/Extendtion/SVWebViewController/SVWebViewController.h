//
//  SVWebViewController.h
//
//  Created by Tran Ngoc Sang on 08.11.10.
//  Copyright 2010 Tran Ngoc Sang. All rights reserved.
//
//  https://github.com/samvermette/SVWebViewController

#import <MessageUI/MessageUI.h>

#import "SVModalWebViewController.h"
#import "MBProgressHUD.h"

@interface SVWebViewController : UIViewController

- (id)initWithAddress:(NSString*)urlString;
- (id)initWithURL:(NSURL*)URL;

@property (nonatomic, readwrite) SVWebViewControllerAvailableActions availableActions;
@property(nonatomic,retain)MBProgressHUD*hud;
@end
