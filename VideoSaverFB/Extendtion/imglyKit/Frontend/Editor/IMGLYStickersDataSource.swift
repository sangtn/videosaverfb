//
//  IMGLYStickersDataSource.swift
//  imglyKit
//
//  Created by Sascha Schwabbauer on 23/03/15.
//  Copyright (c) 2015 9elements GmbH. All rights reserved.
//

import UIKit

public protocol IMGLYStickersDataSourceDelegate: class, UICollectionViewDataSource {
    var stickers: [IMGLYSticker] { get }
}

public class IMGLYStickersDataSource: NSObject, IMGLYStickersDataSourceDelegate {
    public let stickers: [IMGLYSticker]
    
    override init() {
        let stickerFiles = [
            "stk0",
            "stk1",
            "stk2",
            "stk3",
            "stk4",
            "stk5",
            "stk6",
            "stk7",
            "stk8",
            "stk9",
            "stk10",
            "stk11",
            "stk12",
            "stk13",
            "stk14",
            "stk15",
            "stk16",
            "stk17",
            "stk18",
            "stk19",
            "stk20",
            "stk21",
            "stk22",
            "stk23",
            "stk24",
            "stk25",
            "stk26",
            "stk27",
            "stk28",
            "stk29",
            "stk30",
            "stk31",
            "stk32",
            "stk33",
            "stk34",
            "stk35",
            "stk36",
            "stk37",
            "stk38",
            "stk39",
            "stk40",
            "stk41",
            "stk42",
            "stk43",
            "stk44",
            "stk45",
            "stk46",
            "stk47",
            "stk48",
            "stk49",
            "stk50",
            "stk51",
            "stk52",
            "stk53",
            "stk54",
            "stk55",
            "stk56",
            "stk57",
            "stk58",
            "stk59",
            "stk60",
            "stk61",
            "stk62",
            "stk63",
            "stk64",
            "stk65",
            "stk66",
            "stk67",
            "stk68",
            "stk69",
            "stk70",
            "stk71",
            "stk72",
            "stk73",
            "stk74",
            "stk75",
            "stk76",
            "stk77",
            "stk78",
            "stk79",
            "stk80",
            "stk81",
            "stk82",
            "stk83",
            "stk84",
            "stk85",
            "stk86",
            "stk87",
            "stk88",
            "stk89",
            "stk90",
            "stk91",
            "stk92",
            "stk93",
            "stk94",
            "stk95",
            "stk96",
            "stk97",
            "stk98",
            "stk99",
            "stk100",
            "stk101",
            "stk102",
            "stk103",
            "stk104",
            "stk105",
            "stk106",
            "stk107",
            "stk108",
            "stk109",
            "stk110",
            "stk111",
            "stk112",
            "stk113",
            "stk114",
            "stk115",
            "stk116",
            "stk117",
            "stk118",
            "stk119",
            "stk120",
            "stk121",
            "stk122",
            "stk123",
            "stk124",
            "stk125",
            "stk126",
            "stk127",
            "stk128",
            "stk129",
            "stk130",
            "stk131",
            "stk132",
            "stk133",
            "stk134",
            "stk135",
            "stk136",
            "stk137",
            "stk138",
            "stk139"
        ]
        
        stickers = stickerFiles.map { (file: String) -> IMGLYSticker? in
            if let image = UIImage(named: file, inBundle: NSBundle(forClass: IMGLYStickersDataSource.self), compatibleWithTraitCollection: nil) {
                let thumbnail = UIImage(named: file + "_thumbnail", inBundle: NSBundle(forClass: IMGLYStickersDataSource.self), compatibleWithTraitCollection: nil)
                return IMGLYSticker(image: image, thumbnail: thumbnail)
            }
            
            return nil
            }.filter { $0 != nil }.map { $0! }
        
        super.init()
    }
    
    public init(stickers: [IMGLYSticker]) {
        self.stickers = stickers
        super.init()
    }
    
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stickers.count
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(StickersCollectionViewCellReuseIdentifier, forIndexPath: indexPath) as! IMGLYStickerCollectionViewCell
        
        cell.imageView.image = stickers[indexPath.row].thumbnail ?? stickers[indexPath.row].image
        
        return cell
    }
}
