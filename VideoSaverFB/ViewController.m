//
//  ViewController.m
//  VideoSaverFB
//
//  Created by Tran Sang on 9/5/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import "ViewController.h"
#import "AdsManager.h"
#import "VideoSaveFB.pch"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SettingViewController.h"

#import "EHHorizontalSelectionView.h"
#import "UIColor+UIColorAdditions.h"
#import "UIImage+UIImageAdditions.h"

//#import "imglyKit.h"
//#import "VideoSaverFB-Swift.h"

#import "VSFBTextModel.h"
#import "VSFBVideoModel.h"

@interface ViewController ()<GADBannerViewDelegate,UIPageViewControllerDelegate,EHHorizontalSelectionViewProtocol,UIScrollViewDelegate,FBSDKLoginButtonDelegate>{//IMGLYCameraViewControllerDelegate,

    GADBannerView *bannerView_;
    NSArray * _arrTabbar;
    NSArray * _arrVideos;
}
@property (retain, nonatomic) FBSDKLoginButton *loginButton;
@property(nonatomic,retain)UIButton *settingBtn;
@property (weak, nonatomic) IBOutlet EHHorizontalSelectionView *tabbarView;
@property (strong, nonatomic) AVAsset *asset;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
#ifdef INCLUDING_ADS
    [self loadAds];
#endif
}

- (void)setupData {
    [super setupData];
    
    //get video cell data
    NSMutableArray * dLoaderArrData = [NSMutableArray arrayWithArray:[VSFBTextModel dLoaderCellTitleArray]];
    NSMutableArray * uLoaderArrData = [NSMutableArray arrayWithArray:[VSFBTextModel uLoaderCellTitleArray]];;
    
    NSArray * dLoadedArrData = [VSFBFunction getDownloadedVideo];
    [self.tableView_dloaded getDataWithArray:dLoadedArrData];
    [self.tableView_dloader getDataWithArray:dLoaderArrData];
    [self.tableView_uploader getDataWithArray:uLoaderArrData];
    
}

- (void)setupUI {
    [super setupUI];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    
    _loginButton = [[FBSDKLoginButton alloc] init];
    _loginButton.center = self.view.center;
    _loginButton.readPermissions = @[@"public_profile", @"email", @"user_friends",@"user_videos",@"user_likes"];
    _loginButton.delegate = self;
    [self.view addSubview:_loginButton];
    
    _arrTabbar = @[@"Downloaded",@"Downloader", @"Uploader"];
    _tabbarView.delegate = self;
    [_tabbarView registerCellWithClass:[EHHorizontalLineViewCell class]];
    [_tabbarView setTintColor:[UIColor colorWithHex:0xff46c7]];
    _tabbarView.backgroundColor = [UIColor colorWithRed:0.15 green:0.13 blue:0.11 alpha:0.2];
    [EHHorizontalLineViewCell updateColorHeight:3.f];
    [EHRoundedHorizontalViewCell updateTintColor:[UIColor colorWithHex:0xffb647]];
    [EHRoundedHorizontalViewCell updateFontMedium:[UIFont systemFontOfSize:15]];
    [self.view addSubview:_tabbarView];
    _tabbarView.hidden = YES;
    self.scrollView.hidden = YES;
    
    _arrVideos = @[@"Downloaded",@"Downloader", @"Uploader"];
}

-(void)viewWillAppear:(BOOL)animated {
    [self fetchUserInfo];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithStartColor:[UIColor colorWithHex:0x1f1c27] endColor:[UIColor colorWithHex:0x544a57] size:CGSizeMake(1, self.view.bounds.size.height)]];
    
    [super viewWillAppear:animated];
}

-(void)settingsView:(id) sender
{
    SettingViewController *vc = [[SettingViewController alloc]init];
    vc.delegate = self;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}


-(void)fetchUserInfo {
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        NSLog(@"Token is available");
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name,picture.type(large), email"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"fb user info : %@",result);
//                 [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageWithStartColor:[UIColor colorWithHex:0x1f1c27] endColor:[UIColor colorWithHex:0x544a57] size:CGSizeMake(1, 64)]]];
//                 //--- Settings
                 _settingBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                 [_settingBtn setFrame:CGRectMake(3, 25, 40.f, 40.f)];
                 [_settingBtn setImage:[UIImage imageNamed:@"Setting.bundle/video-setting"] forState:UIControlStateNormal];
                 _settingBtn.tintColor = [UIColor whiteColor];
                 [_settingBtn addTarget:self action:@selector(settingsView:) forControlEvents:UIControlEventTouchUpInside];
                 
                 UIBarButtonItem * settingBarBtn = [[UIBarButtonItem alloc]initWithCustomView:_settingBtn];
                 
//                 [self.view addSubview:_settingBtn];
                 

                 
                 _loginButton.frame = CGRectMake(self.view.frame.size.width-90, 28, 85, 35);
//                 _navBar.hidden = NO;
                 
                 self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:_loginButton];
//                 UINavigationItem *navTitle = [[UINavigationItem alloc] initWithTitle:result[@"name"]];
                 self.title = result[@"name"];
//                 navTitle.hidesBackButton = YES;
                 
                 self.navigationItem.hidesBackButton = YES;
                 
//                 [_navBar setItems:[NSArray arrayWithObject:navTitle] animated:YES];
                 
                 UIImageView *imgProfile = [[UIImageView alloc]initWithFrame: CGRectMake(50, 25, 40, 40)];
                 imgProfile.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:result[@"picture"][@"data"][@"url"]]]];
//                 [self.view addSubview:imgProfile];
                 UIBarButtonItem * imgBtn = [[UIBarButtonItem alloc]initWithCustomView:imgProfile];
                 self.navigationItem.leftBarButtonItems = @[settingBarBtn,imgBtn];
                 [self.navigationController setNavigationBarHidden:NO animated:YES];
                 
                 _tabbarView.hidden = NO;
                 self.scrollView.hidden = NO;
             }
             else
             {
                 NSLog(@"error : %@",error);
             }
         }];
        
    } else {
        
        NSLog(@"User is not Logged in");
    }
}

//-(void)IMGLYCameraViewControllerDidFinish {
//    
//    [self startMediaBrowserFromViewController: self
//                                usingDelegate: self];
//    
//}

-(void)loadAds
{
#ifdef INCLUDING_ADS
    GADRequest *request = [GADRequest request];
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        [bannerView_ setFrame:CGRectMake(bannerView_.frame.origin.x, self.view.frame.size.height-90, self.view.frame.size.width, bannerView_.frame.size.height)];
    }else{
        bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
        [bannerView_ setFrame:CGRectMake(bannerView_.frame.origin.x, self.view.frame.size.height-115, self.view.frame.size.width, bannerView_.frame.size.height)];
    }
    bannerView_.adUnitID = ADMOBID;
    bannerView_.delegate = self;
    bannerView_.rootViewController = self;
    [bannerView_ loadRequest:request];
    [self.view addSubview:bannerView_];
#endif
}



- (void)horizontalSelection:(EHHorizontalSelectionView * _Nonnull)hSelView didSelectObjectAtIndex:(NSUInteger)index{
    NSLog(@"tab choose %lu",(unsigned long)index);
    
    [self.scrollView scrollRectToVisible:CGRectMake(index * self.tableView_dloaded.frame.size.width, 0, self.tableView_dloaded.frame.size.width, self.tableView_dloaded.frame.size.height) animated:YES];
}

#pragma mark - EHHorizontalSelectionViewProtocol
- (NSUInteger)numberOfItemsInHorizontalSelection:(EHHorizontalSelectionView*)hSelView
{
    if (hSelView == _tabbarView)
    {
        return [_arrTabbar count];
    }
    
    return 0;
}

- (NSString *)titleForItemAtIndex:(NSUInteger)index forHorisontalSelection:(EHHorizontalSelectionView*)hSelView
{
    if (hSelView == _tabbarView)
    {
        //return [[_arrTabbar objectAtIndex:index] uppercaseString];
        return [_arrTabbar objectAtIndex:index];
    }
    return @"";
}

#pragma mark - UI

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
#pragma mark - UIScrollView
- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView != self.scrollView) {
        return;
    }
    
    CGFloat xPosition = scrollView.contentOffset.x;
    if (xPosition<0 || xPosition > self.view.frame.size.width*2) {
        return;
    }
    
    CGFloat width = scrollView.frame.size.width;
    NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
    
//    [self horizontalSelection:self.tabbarView didSelectObjectAtIndex:page];
    [self.tabbarView selectIndex:page];
    
}

#pragma mark - FacebookLoginButtonDelegate
- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
    
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [FBSDKProfile setCurrentProfile:nil];
    _tabbarView.hidden = YES;
    self.scrollView.hidden = YES;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.loginButton.center = self.view.center;
}

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    NSLog(@"RESULT :%@--- ERROR :%@",result,error);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
