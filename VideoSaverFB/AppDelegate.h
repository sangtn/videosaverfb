//
//  AppDelegate.h
//  VideoSaverFB
//
//  Created by Tran Sang on 9/5/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

