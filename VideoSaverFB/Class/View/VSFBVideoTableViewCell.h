//
//  VSFBVideoTableViewCell.h
//  VideoSaverFB
//
//  Created by Macbook Pro on 9/15/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "ASIHTTPRequest.h"

@class VSFBVideoTableViewCell;
@protocol VSFBVideoTableViewCellDelegate <NSObject>

- (void) downloadVideoWithProgress : (CGFloat) progress;
- (void) completeDownload;
@end


@interface VSFBVideoTableViewCell : BaseTableViewCell <ASIProgressDelegate,ASIHTTPRequestDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *thumbImage;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_duration;
@property (weak, nonatomic) IBOutlet UIButton *btn_download;
@end
