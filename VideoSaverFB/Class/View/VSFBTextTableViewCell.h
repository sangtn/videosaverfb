//
//  VSFBTextTableViewCell.h
//  VideoSaverFB
//
//  Created by Macbook Pro on 9/15/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface VSFBTextTableViewCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView_thumb;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@end
