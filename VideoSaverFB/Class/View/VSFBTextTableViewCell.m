//
//  VSFBTextTableViewCell.m
//  VideoSaverFB
//
//  Created by Macbook Pro on 9/15/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import "VSFBTextTableViewCell.h"
#import "VSFBTextModel.h"

@implementation VSFBTextTableViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)fillData:(id)data withIndex:(NSIndexPath *)index {
    VSFBTextModel * textModel = (VSFBTextModel*)data;
    self.lbl_title.text = textModel.titleObject;
    self.imgView_thumb.image = [UIImage imageNamed:textModel.thumbImage];
}
@end
