//
//  VSFBVideoTableViewCell.m
//  VideoSaverFB
//
//  Created by Macbook Pro on 9/15/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import "VSFBVideoTableViewCell.h"
#import "VSFBVideoModel.h"
#import "UIImageView+WebCache.h"
#import "VSFBVideoViewController.h"
#import "AdsManager.h"

#define NUMBER_WORKER 5
#define NUMBER_CHUNK 5

@implementation VSFBVideoTableViewCell
{
    VSFBVideoModel * videoModel;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)fillData:(id)data withIndex:(NSIndexPath *)index {
    videoModel = (VSFBVideoModel *) data;
    [self.thumbImage sd_setImageWithURL:[NSURL URLWithString:videoModel.thumbImage] placeholderImage:[UIImage imageNamed:@""]];
    self.lbl_title.text = videoModel.titleObject;
}


- (IBAction)action_download:(id)sender {
    [[AdsManager sharedInstance] ShowAdsFullScreen];
    NSLog(@"DOWNLOAD WITH URL :%@",videoModel.streamURL);
    VSFBVideoViewController * vc = (VSFBVideoViewController *)[VSFBFunction getCurrentViewController];
    
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"Do you want to download this video" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * downloadAction = [UIAlertAction actionWithTitle:@"Download" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.btn_download animated:YES];
//        hud.mode = MBProgressHUDModeAnnularDeterminate;
        
        NSArray * path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * documentPath = [path objectAtIndex:0];
        NSString * fileName = [NSString stringWithFormat:@"%@.mp4",videoModel.titleObject];
        NSString * filePath = [[documentPath stringByAppendingPathComponent:@"VideoSaverFB"] stringByAppendingPathComponent:fileName];
        ASIHTTPRequest * request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:videoModel.streamURL]];
        request.delegate = self;
        request.downloadProgressDelegate = self;
        [request setNumberOfTimesToRetryOnTimeout:5];
        [request setTimeOutSeconds:60];
        [request setDownloadDestinationPath:filePath];
        
        [request startAsynchronous];
        
    }];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:downloadAction];
    [vc presentViewController:alertController animated:YES completion:NULL];
}

#pragma mark - ASIHTTPRequestDelegate
- (void)requestStarted:(ASIHTTPRequest *)request {
    
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    NSLog(@"REQUEST FAILT :%@",request.error.userInfo);
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    NSLog(@"REQUEST :%@",request.error);
}
#pragma mark - ASIProgressDelegate
- (void)setProgress:(float)newProgress {
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.btn_download animated:YES];
    hud.mode = MBProgressHUDModeDeterminate;
    hud.progress = newProgress;
    if (newProgress > 0.99) {
        [MBProgressHUD hideHUDForView:self.btn_download animated:YES];
    }
    NSLog(@"PROGRESS :%f",newProgress);
}
#pragma mark - UIAlertViewDelegate
@end
