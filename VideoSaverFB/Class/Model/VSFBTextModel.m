//
//  VSFBTextModel.m
//  VideoSaverFB
//
//  Created by Macbook Pro on 9/15/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import "VSFBTextModel.h"
#import "VSFBVideoModel.h"
#import "ViewController.h"
#import "VSFBVideoViewController.h"
#import <CoreMedia/CoreMedia.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <Social/Social.h>

@import FBSDKCoreKit;
@import UIKit;

@implementation VSFBTextModel

- (instancetype)initwithAtrributes:(NSDictionary *)attributes {
    if (self == [super initwithAtrributes:attributes]) {
        self.titleObject = attributes[@"name"];
    }
    return self;
}

+ (NSArray *)dLoaderCellTitleArray {
    NSMutableArray * dataArray = [[NSMutableArray alloc]init];
    VSFBTextModel * model = [[VSFBTextModel alloc]init];
    model.titleObject = @"Your Own Facebook Videos";
    model.thumbImage = @"uploaded_video.png";
    [dataArray addObject:model];
    
    model = [[VSFBTextModel alloc]init];
    model.titleObject = @"Your Own Tagged Videos";
    model.thumbImage = @"tagged_video.png";
    [dataArray addObject:model];
    
    model = [[VSFBTextModel alloc]init];
    model.titleObject = @"Like Page Videos";
    model.thumbImage = @"likepage_video.png";
    [dataArray addObject:model];
    
    return dataArray;
}

+ (NSArray *)uLoaderCellTitleArray {
    NSMutableArray * dataArray = [[NSMutableArray alloc]init];
    VSFBTextModel * model = [[VSFBTextModel alloc]init];
    model.titleObject = @"Video From Camera Roll";
    model.thumbImage = @"library_video.png";
    [dataArray addObject:model];
    
    model = [[VSFBTextModel alloc]init];
    model.titleObject = @"Create New Videos";
    model.thumbImage = @"live_camera.png";
    [dataArray addObject:model];
    
    return dataArray;
}



- (NSString *)getCellIdentifier {
    static NSString * cellIdentifier = @"VSFBTextTableViewCell";
    return cellIdentifier;
}

- (void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath andViewController:(UIViewController *)viewController {
    NSMutableArray * arrData = [[NSMutableArray alloc]init];
    ViewController * rootVC = (ViewController *)viewController;
    
    if ([self.titleObject isEqualToString:@"Your Own Facebook Videos"]) {
        
        NSLog(@"VAO Your Own Facebook Videos");
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                      initWithGraphPath:@"/me/videos/uploaded"
                                      parameters:@{@"fields":@"format,description,source"}
                                      HTTPMethod:@"GET"];
        
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result,
                                              NSError *error) {
            NSLog(@"result Uploaded:%@",result);
            // Handle the result
            NSArray * arrObject = result[@"data"];
            for (NSDictionary * dict in arrObject) {
                
                VSFBVideoModel * model = [[VSFBVideoModel alloc]initwithAtrributes:dict];
                [arrData addObject:model];
                
            }
            
            VSFBVideoViewController * vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([VSFBVideoViewController class])];
            vc.dataArray = arrData;
            vc.pageTitle = @"Your Own Facebook Videos";
            [rootVC.navigationController pushViewController:vc animated:YES];
        }];
    }else if ([self.titleObject isEqualToString:@"Your Own Tagged Videos"]){
        NSLog(@"VAO Your Own Tagged Videos");
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                      initWithGraphPath:@"/me/videos"
                                      parameters:@{@"fields":@"format,description,source"}
                                      HTTPMethod:@"GET"];
        
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result,
                                              NSError *error) {
            NSLog(@"result TAG :%@",result);
            NSArray * arrObject = result[@"data"];
            for (NSDictionary * dict in arrObject) {
                
                VSFBVideoModel * model = [[VSFBVideoModel alloc]initwithAtrributes:dict];
                [arrData addObject:model];
                
            }
            
            VSFBVideoViewController * vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([VSFBVideoViewController class])];
            vc.dataArray = arrData;
            vc.pageTitle = @"Your Own Tagged Videos";
            [rootVC.navigationController pushViewController:vc animated:YES];
            // Handle the result
        }];
    }else if ([self.titleObject isEqualToString:@"Like Page Videos"]){
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                      initWithGraphPath:@"/me/likes"
                                      parameters:@{@"fields":@"name"}
                                      HTTPMethod:@"GET"];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result,
                                              NSError *error) {
            // Handle the result
            NSLog(@"RESULT LIKE PAGE:%@",result);
            NSArray * arrObject = result[@"data"];
            for (NSDictionary * dict in arrObject) {
                VSFBTextModel * model = [[VSFBTextModel alloc]initwithAtrributes:dict];
                [arrData addObject:model];
            }
            
            VSFBVideoViewController * vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([VSFBVideoViewController class])];
            vc.dataArray = arrData;
            vc.pageTitle = @"Like Page Videos";
            [rootVC.navigationController pushViewController:vc animated:YES];
        }];
        NSLog(@"Like Page Videos");
    }else if ([self.titleObject isEqualToString:@"Video From Camera Roll"]){
        NSLog(@"VAO Create New Videos");
        [self startMediaBrowserFromViewController: [VSFBFunction getCurrentViewController]
                                    usingDelegate: self];
    }else if ([self.titleObject isEqualToString:@"Create New Videos"]){
        NSLog(@"VAO 5");
        UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
        pickerController.delegate = self;
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        pickerController.showsCameraControls = YES;
        pickerController.mediaTypes = [NSArray arrayWithObject:(id)kUTTypeMovie];
        
        UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
            
            viewController = viewController.presentedViewController;
            
        }
        [viewController presentViewController:pickerController animated:YES completion:nil];
    }else {//case like page
        NSLog(@"CASE LIKE PAGE WITH NAME :%@",self.titleObject);
        NSString * graphPath  = [NSString stringWithFormat:@"/%@/videos/uploaded",self.identifierObject];
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                      initWithGraphPath:graphPath
                                      parameters:@{@"fields":@"format,description,source,published"}
                                      HTTPMethod:@"GET"];
        
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result,
                                              NSError *error) {
            NSLog(@"result Uploaded:%@",result);
            // Handle the result
            NSArray * arrObject = result[@"data"];
            for (NSDictionary * dict in arrObject) {
                if ([dict[@"published"] boolValue]) {
                    VSFBVideoModel * model = [[VSFBVideoModel alloc]initwithAtrributes:dict];
                    [arrData addObject:model];
                }
            }
            
            VSFBVideoViewController * vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([VSFBVideoViewController class])];
            vc.dataArray = arrData;
            vc.pageTitle = self.titleObject;
            [rootVC.navigationController pushViewController:vc animated:YES];
        }];
    }
    
}
- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate {
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = delegate;
    
    [controller presentViewController:mediaUI animated:YES completion:nil];
    
    return YES;
}
- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info {
    AVURLAsset *asset = [AVURLAsset assetWithURL:[info objectForKey:UIImagePickerControllerReferenceURL]];
    
    [[VSFBFunction getCurrentViewController] dismissViewControllerAnimated:YES completion:^{
        ViewController * rootVC = (ViewController *)[VSFBFunction getCurrentViewController];
        SLComposeViewController *vc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result)
        {
            [vc dismissViewControllerAnimated:YES completion:nil];
            switch(result)
            {
                    
                case SLComposeViewControllerResultCancelled:
                    //                [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [self deleteTempFileWithUrl:[asset URL]];
                default:
                    break;
                case SLComposeViewControllerResultDone:
                    //                [self registerHudWithText:@"Successfull share to facebook"];
                    [self deleteTempFileWithUrl:[asset URL]];
                    
                    break;
            }
        };
        [vc addURL:[asset URL]];
        [vc setInitialText:@""];
        [vc setCompletionHandler:completionHandler];
        [rootVC presentViewController:vc animated:YES completion:NULL];
    }];
    NSLog(@"videochoose: %@",asset);
    
}

- (void)deleteTempFileWithUrl : (NSURL *) asset
{
    NSURL *url = asset;
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL exist = [fm fileExistsAtPath:url.path];
    NSError *err;
    if (exist) {
        [fm removeItemAtURL:url error:&err];
        NSLog(@"file deleted");
        
        if (err) {
            NSLog(@"file remove error, %@", err.localizedDescription );
        }
    } else {
        NSLog(@"no file by that name");
    }
    
}



@end
