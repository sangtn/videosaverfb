//
//  VSFBVideoModel.m
//  VideoSaverFB
//
//  Created by Macbook Pro on 9/15/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import "VSFBVideoModel.h"
#import "ViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation VSFBVideoModel

- (NSString *)getCellIdentifier {
    static NSString * cellIdentifier = @"VSFBVideoTableViewCell";
    return cellIdentifier;
}

- (void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath andViewController:(UIViewController *)viewController {
    NSLog(@"VIDEO STREAM URL :%@",self.streamURL);
    ViewController * rootVC = (ViewController *)viewController;
    MPMoviePlayerViewController * vc = [[MPMoviePlayerViewController alloc]initWithContentURL:[NSURL URLWithString:self.streamURL]];
    [rootVC presentMoviePlayerViewControllerAnimated:vc];
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.identifierObject forKey:@"identifierObject"];
    [encoder encodeObject:self.titleObject forKey:@"titleObject"];
    [encoder encodeObject:self.subTitleObject forKey:@"subTitleObject"];
    [encoder encodeObject:self.thumbImage forKey:@"thumbImage"];
    [encoder encodeObject:self.recentTime forKey:@"recentTime"];
    [encoder encodeObject:self.numberView forKey:@"numberView"];
    [encoder encodeObject:[NSNumber numberWithInteger:self.duration] forKey:@"duration"];
    [encoder encodeObject:self.streamURL forKey:@"streamURL"];
    [encoder encodeObject:[NSNumber numberWithBool:self.isDownloaded] forKey:@"isDownloaded"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.identifierObject = [decoder decodeObjectForKey:@"identifierObject"];
        self.titleObject = [decoder decodeObjectForKey:@"titleObject"];
        self.subTitleObject = [decoder decodeObjectForKey:@"subTitleObject"];
        self.thumbImage = [decoder decodeObjectForKey:@"thumbImage"];
        self.recentTime = [decoder decodeObjectForKey:@"recentTime"];
        self.numberView = [decoder decodeObjectForKey:@"numberView"];
        self.duration = [[decoder decodeObjectForKey:@"duration"] integerValue];
        self.streamURL = [decoder decodeObjectForKey:@"streamURL"];
        self.isDownloaded = [[decoder decodeObjectForKey:@"isDownloaded"] boolValue];
    }
    return self;
}
- (void)saveCustomObject:(VSFBFunction *)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
}

- (VSFBFunction *)loadCustomObjectWithKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    VSFBFunction *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

@end
