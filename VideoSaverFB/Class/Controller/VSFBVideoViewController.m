//
//  VSFBVideoViewController.m
//  VideoSaverFB
//
//  Created by Macbook Pro on 9/22/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import "VSFBVideoViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface VSFBVideoViewController ()

@end

@implementation VSFBVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupUI {
    [super setupUI];
    self.title = self.pageTitle;
}


- (void)setupData {
    [super setupData];
    [self.tableView setDataArray:self.dataArray];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
