//
//  BaseTableView.m
//  Baseproject
//
//  Created by Nhat Vu on 7/16/16.
//  Copyright © 2016 Nhat Vu. All rights reserved.
//

#import "BaseTableView.h"
#import "BaseViewController.h"
#import "BaseTableViewCell.h"
#import "BaseModel.h"
#import "MBProgressHUD.h"
#import "SVPullToRefresh.h"

@interface BaseTableView()

@end

@implementation BaseTableView

- (void)awakeFromNib {
    
    [self setHasPullToRefresh:self.hasPullToRefresh];
    [self setHasLoadMore:self.hasLoadMore];
    
    
    
    [super awakeFromNib];
    self.dataSource = self;
    self.delegate = self;
    self.dataArray = [[NSMutableArray alloc]init];
    self.headerArray = [[NSMutableArray alloc]init];
    self.contentInset = UIEdgeInsetsMake(0, 0, 100.0f, 0);
    [self caclulatedOffset];
    
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setHasPullToRefresh:self.hasPullToRefresh];
        [self setHasLoadMore:self.hasLoadMore];
        
        self.dataSource = self;
        self.delegate = self;
        self.dataArray = [[NSMutableArray alloc]init];
        self.headerArray = [[NSMutableArray alloc]init];
        [self caclulatedOffset];
    }
    return self;
}

- (void) caclulatedOffset {
    self.offset = 50;
}


- (void)setHasPullToRefresh:(BOOL)hasPullToRefresh {
    __weak BaseTableView * weakSelf = self;
    if (hasPullToRefresh) {
        [self addPullToRefreshWithActionHandler:^{
            NSLog(@"Pull to refresh");
            [weakSelf getDataWithPath:weakSelf.curPath andParam:weakSelf.curParam isRefresh:YES];
        }];
    }
}

- (void)setHasLoadMore:(BOOL)hasLoadMore{
    __weak BaseTableView * weakSelf = self;
    if (hasLoadMore) {
        [self addInfiniteScrollingWithActionHandler:^{
            NSLog(@"Load more");
            [weakSelf getDataWithPath:weakSelf.curPath andParam:weakSelf.curParam isRefresh:NO];
        }];
    }
    self.infiniteScrollingView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
}

#pragma mark - Method
- (void)getDataWithPath:(NSString *)path andParam:(NSDictionary *)param isRefresh:(BOOL)isRefresh{
    self.curPath = path;
    self.curParam = param;
    if (!isRefresh) {
        NSMutableDictionary * dictParam = [NSMutableDictionary dictionaryWithDictionary:param];
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        NSString * nextPageToken = [defaults objectForKey:@"nextPageToken"];
        if (nextPageToken) {
            [dictParam setObject:nextPageToken forKey:@"pageToken"];
            NSLog(@"TRUE NEXT PAGE TOKEN :%@",nextPageToken);
            param = dictParam;
        }else {
            [self.infiniteScrollingView stopAnimating];
            return;
        }
    }else {
        [MBProgressHUD showHUDAddedTo:self animated:YES];
    }
    /*
    [STRequest fetchDataWithPath:path AndParam:param withOffset:self.offset withComplitionBLock:^(id data, NSString *message) {
        [MBProgressHUD hideHUDForView:self animated:YES];
        
        if (isRefresh) {
            NSMutableArray * removedArray = [[NSMutableArray alloc]init];
            if (self.dataArray && self.dataArray.count >0) {
                for (BaseModel * model in self.dataArray) {
                    [removedArray addObject:model];
                }
                [self.dataArray removeObjectsInArray:removedArray];
                [self reloadData];
            }
            [self.pullToRefreshView stopAnimating];
            
        }else {
            [self.infiniteScrollingView stopAnimating];
        }
        
        if ([data count]==0) {
            self.hasLoadMore = NO;
        }
        
        for (BaseModel * model in data) {
            [self.dataArray addObject:model];
        }
        
        [self reloadData];
        
        if (self.pullTorfBlock) {
            self.pullTorfBlock();
        }
        
        if (self.loadMoreBlock) {
            self.loadMoreBlock();
        }
    }];*/
}

-(void)getDataWithArray:(NSArray *)arrData {
    if (self.dataArray && self.dataArray.count > 0) {
        [self.dataArray removeAllObjects];
    }
    
    for (BaseModel * model in arrData) {
        [self.dataArray addObject:model];
    }
    [self reloadData];
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    BaseModel * model = [self.dataArray firstObject];
    return [model getCellHeight];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BaseModel * rootModel = [self.dataArray firstObject];
    BaseTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[rootModel getCellIdentifier]];
    BaseModel * model;
    model = [self.dataArray objectAtIndex:indexPath.row];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:[rootModel getCellIdentifier] owner:self
                                            options:nil] firstObject];
    }
    
    [cell fillData:model withIndex:indexPath];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BaseModel * model = [self.dataArray objectAtIndex:indexPath.row];
    [model didSelectRowAtIndexPath:indexPath andViewController:[VSFBFunction getCurrentViewController]];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    BaseModel * model = [self.dataArray objectAtIndex:indexPath.row];
    [model didDeselectRowAtIndexPath:indexPath andViewController:[VSFBFunction getCurrentViewController]];
}

@end
