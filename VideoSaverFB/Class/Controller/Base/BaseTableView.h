//
//  BaseTableView.h
//  Baseproject
//
//  Created by Nhat Vu on 7/16/16.
//  Copyright © 2016 Nhat Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^compliHandle)(id data, NSString * message);
typedef void(^pullToRefreshBlock)();
typedef void(^loadMoreBlock)();

@interface BaseTableView : UITableView <UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) NSMutableArray * dataArray;
@property (strong, nonatomic) NSMutableArray * headerArray;
@property (assign, nonatomic) NSInteger offset;

@property (strong, nonatomic) NSString * curPath;
@property (strong, nonatomic) NSDictionary * curParam;

@property (nonatomic) BOOL hasPullToRefresh;
@property (nonatomic) BOOL hasLoadMore;
@property (strong, nonatomic) compliHandle completeHandle;
@property (strong, nonatomic) pullToRefreshBlock pullTorfBlock;
@property (strong, nonatomic) loadMoreBlock loadMoreBlock;

- (void) getDataWithPath:(NSString *) path andParam: (NSDictionary *) param isRefresh :(BOOL) isRefresh;
- (void) getDataWithArray : (NSArray *) arrData;
@end
