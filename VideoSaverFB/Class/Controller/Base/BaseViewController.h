//
//  BaseViewController.h
//  Baseproject
//
//  Created by Nhat Vu on 7/12/16.
//  Copyright © 2016 Nhat Vu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STLostconnectionView.h"
#import "Reachability.h"


@interface BaseViewController : UIViewController <STLostconnectionViewDelegate>
@property (assign, nonatomic) BOOL needShowLostConnectionView;
@property (strong, nonatomic) STLostconnectionView * lostConnectionView;
@property (strong, nonatomic) Reachability *reachability;
@property (assign, nonatomic) NetworkStatus internetStatus;

- (void) setupUI;
- (void) setupData;
- (void) refreshData;
- (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated completion:(void (^)(BOOL))completion;
@end
