//
//  BaseModel.h
//  Baseproject
//
//  Created by Nhat Vu on 7/12/16.
//  Copyright © 2016 Nhat Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface BaseModel : NSObject
@property (strong, nonatomic) NSString * identifierObject;
@property (strong, nonatomic) NSString * titleObject;
@property (strong, nonatomic) NSString * subTitleObject;
@property (strong, nonatomic) NSString * recentTime;
@property (strong, nonatomic) NSString * numberView;
@property (strong, nonatomic) NSString * thumbImage;
@property (assign, nonatomic) NSInteger duration;
@property (strong, nonatomic) NSString * streamURL;


- (instancetype) initwithAtrributes : (NSDictionary *) attributes;


- (void) didSelectRowAtIndexPath: (NSIndexPath *) indexPath andViewController : (UIViewController *) viewController;
- (void) didDeselectRowAtIndexPath: (NSIndexPath*) indexPath andViewController : (UIViewController*) viewController;
- (NSString *) getCellIdentifier;
- (CGFloat) getCellHeight;
@end
