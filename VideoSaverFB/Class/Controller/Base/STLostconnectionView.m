//
//  STLostconnectionView.m
//  Baseproject
//
//  Created by Macbook Pro on 8/18/16.
//  Copyright © 2016 Nhat Vu. All rights reserved.
//

#import "STLostconnectionView.h"

@implementation STLostconnectionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

//- (instancetype)initWithCoder:(NSCoder *)aDecoder {
//    self = [super initWithCoder:aDecoder];
//    if (self) {
//        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
//    }
//    return self;
//}

- (IBAction)action_retry:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(retryLoadData)]) {
        [self.delegate retryLoadData];
    }
}
@end
