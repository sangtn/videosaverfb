//
//  BaseTableViewCell.m
//  Baseproject
//
//  Created by Nhat Vu on 7/12/16.
//  Copyright © 2016 Nhat Vu. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillData:(id)data withIndex:(NSIndexPath *)index {
    
}

@end
