//
//  BaseViewController.m
//  Baseproject
//
//  Created by Nhat Vu on 7/12/16.
//  Copyright © 2016 Nhat Vu. All rights reserved.
//

#import "BaseViewController.h"


@interface BaseViewController ()
@end

@implementation BaseViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    Reachability *reachabilityInfo;
    Reachability *internetReachable = [Reachability reachabilityForInternetConnection];
    // This will tell the notifier to start sending notifications
    [internetReachable startNotifier];
    [[NSNotificationCenter defaultCenter] postNotificationName:kReachabilityChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeNetworkNotification:)
                                                 name:kReachabilityChangedNotification
                                               object:reachabilityInfo];
    
    self.needShowLostConnectionView = YES;
    [self checkNetwork];
    [self setupData];
    [self setupUI];
    [self refreshData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) changeNetworkNotification: (NSNotification *) notification {
    self.reachability = [Reachability reachabilityForInternetConnection];
    self.internetStatus = [self.reachability currentReachabilityStatus];
    if (self.internetStatus != NotReachable) {
        for (UIView * view in self.view.subviews) {
            if ([view isKindOfClass:[STLostconnectionView class]]) {
                [view removeFromSuperview];
            }
        }
//        [self setupData];
    }
    else {
        //hien thi view lostconnection
        if (self.needShowLostConnectionView) {
            CGRect frame = self.lostConnectionView.frame;
            frame.origin.x = 0;
            frame.origin.y = 0;
            frame.size = self.view.frame.size;
            self.lostConnectionView.frame = frame;
            [self.view addSubview:self.lostConnectionView];
            [self.view bringSubviewToFront:self.lostConnectionView];
        }
    }
}

- (void) checkNetwork {
    self.reachability = [Reachability reachabilityForInternetConnection];
    self.internetStatus = [self.reachability currentReachabilityStatus];
    if (self.internetStatus != NotReachable) {
        for (UIView * view in self.view.subviews) {
            if ([view isKindOfClass:[STLostconnectionView class]]) {
                [view removeFromSuperview];
            }
        }
        [self setupData];
    }
    else {
        if (self.needShowLostConnectionView) {
            CGRect frame = self.lostConnectionView.frame;
            frame.origin.x = 0;
            frame.origin.y = 0;
            frame.size = self.view.frame.size;
            self.lostConnectionView.frame = frame;
            [self.view addSubview:self.lostConnectionView];
            
            [self.view bringSubviewToFront:self.lostConnectionView];
        }
    }
    
}

- (void)setupUI {
    self.lostConnectionView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([STLostconnectionView class]) owner:self options:nil] firstObject];
    self.lostConnectionView.delegate = self;
}

- (void)setupData {
    
}

- (void)refreshData {
    
}

#pragma hide tabbar animation
- (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated completion:(void (^)(BOOL))completion {
    
    // bail if the current state matches the desired state
    if ([self tabBarIsVisible] == visible) return (completion)? completion(YES) : nil;
    
    // get a frame calculation ready
    CGRect frame = self.tabBarController.tabBar.frame;
    CGFloat height = frame.size.height;
    CGFloat offsetY = (visible)? -height : height;
    
    // zero duration means no animation
    CGFloat duration = (animated)? 0.3 : 0.0;
    
    [UIView animateWithDuration:duration animations:^{
        self.tabBarController.tabBar.frame = CGRectOffset(frame, 0, offsetY);
    } completion:completion];
}

// know the current state
- (BOOL)tabBarIsVisible {
    return self.tabBarController.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame);
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - LostconnectionViewDelegate
-(void)retryLoadData {
    [self checkNetwork];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
