//
//  BaseModel.m
//  Baseproject
//
//  Created by Nhat Vu on 7/12/16.
//  Copyright © 2016 Nhat Vu. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel
- (instancetype)initwithAtrributes:(NSDictionary *)attributes {
    if (self) {
        self.identifierObject = attributes[@"id"];
        self.titleObject= attributes[@"description"];
        self.thumbImage = attributes[@"format"][0][@"picture"];
        self.streamURL = attributes[@"source"];
    }
    return self;
}

-(void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath andViewController:(UIViewController *)viewController{
    
}

- (void) didDeselectRowAtIndexPath: (NSIndexPath*) indexPath andViewController : (UIViewController*) viewController {
    
}


- (NSString *)getCellIdentifier {
    return nil;
}

-(CGFloat)getCellHeight {
    return 86.0f;
}
@end
