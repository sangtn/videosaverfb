//
//  STLostconnectionView.h
//  Baseproject
//
//  Created by Macbook Pro on 8/18/16.
//  Copyright © 2016 Nhat Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class STLostconnectionView;
@protocol STLostconnectionViewDelegate <NSObject>
@optional
- (void) retryLoadData;

@end

@interface STLostconnectionView : UIView


@property (assign, nonatomic) id<STLostconnectionViewDelegate> delegate;
- (IBAction)action_retry:(id)sender;
@end
