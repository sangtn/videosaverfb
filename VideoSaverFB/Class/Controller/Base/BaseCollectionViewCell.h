//
//  BaseCollectionViewCell.h
//  Baseproject
//
//  Created by Nhat Vu on 7/12/16.
//  Copyright © 2016 Nhat Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCollectionViewCell : UICollectionViewCell
- (void) fillData : (id) data withIndex : (NSIndexPath *) indexPath;
@end
