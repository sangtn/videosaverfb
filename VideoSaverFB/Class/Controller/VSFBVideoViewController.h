//
//  VSFBVideoViewController.h
//  VideoSaverFB
//
//  Created by Macbook Pro on 9/22/16.
//  Copyright © 2016 Tran Sang. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseTableView.h"

@interface VSFBVideoViewController : BaseViewController
@property (weak, nonatomic) IBOutlet BaseTableView *tableView;
@property (strong, nonatomic) NSMutableArray * dataArray;
@property (strong, nonatomic) NSString * pageTitle;

@end
